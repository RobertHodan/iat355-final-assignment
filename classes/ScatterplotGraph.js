// References:
/*
    D3V4 Scatterplot
        https://bl.ocks.org/sebg/6f7f1dd55e0c52ce5ee0dac2b2769f4b
*/
/*
    Class dependencies:
        Graph.js
        Tooltip.js
*/
/*
    Purpose:
        Creates a basic line graph, with a x and y axis and their respective labels.
*/
class ScatterplotGraph extends Graph {
    constructor(options) {
        super()

        this._setupVariables(options);
        this._initializeSvg();
        this._initializeXLabel();
        this._initializeXAxis();
        this._initializeYLabel();
        this._initializeYAxis();
        this.updateData();
    }

    setData(options) {
        this._setupVariables(options);

        this.updateAll();
    }

    sortBy(string, value) {
        let func;
        if (string === 'greatest') {
            func = function(a, b) {
                return b[value] - a[value];
            }
        }
        else if (string === 'least') {
            func = function(a, b) {
                return a[value] - b[value];
            }
        }

        let options = {};
        options.data = this.dataContainer;
        options.data.rows = this.data.sort(func);
        this._setupVariables(options);
        this.updateAll();
    }

    updateAll() {
        this.updateData();
        this.updateXAxis();
        this.updateYAxis();
    }

    // Purpose:
    //      Updates the bars
    //
    // Parameters:
    //      data: array of values
    //
    // Returns:
    //      void
    updateData() {
        let data = this.data;

        // Select all the rects, within the svg object
        let bars = this.svg
            .selectAll('circle')
            .data(data)

        // Discard data that is no longer used
        bars.exit().remove();

        let that = this;
        // Update the data
        bars.enter()
            .append('circle')
            .merge(bars)
            .transition(this.transition)
            .attr('width', this._getBarSize('width'))
            .attr('height', this._getBarSize('height'))
            .attr('r', 6)
            .attr('cx', (data, i) => {
                return this.x(data[this.xAxis.key]);
            })
            .attr('cy', (data) => {
                return this.y(data[this.yAxis.key]);
            })
            .attr('fill', (data) => {
                let neighbourhood = data[that.circles.key];
                let colour = window.legendColours[neighbourhood];

                // https://stackoverflow.com/questions/3641836/javascript-to-get-alpha-value-from-hex
                var num = parseInt(colour.slice(1), 16);
                let rgba = [num >> 16 & 255, num >> 8 & 255, num & 255, num >> 24 & 255];

                if (that.neighbourhoodColourMap[neighbourhood] || Object.keys(that.neighbourhoodColourMap).length === 0) {
                    rgba[3] = 1;
                } else {
                    rgba[3] = 0.25;
                }
                return 'rgba('+rgba.toString()+')';
            })
            .each(function(data) {
                that.barMap[data[that.yAxis.key]] = this;
            })

        this.svg.selectAll('circle')
            .on('mousemove', function(data, index, rects) {
                let coords = d3.mouse(this);
                let bodyRect = document.body.getBoundingClientRect();
                let elRect = this.getBoundingClientRect();
                that.tooltip.moveTo(elRect.left - bodyRect.left + 25,
                                    elRect.top - bodyRect.top + 35);
                let format = d3.format(",");
                let number = format(data[that.xAxis.key]);
                that.tooltip.setData( {
                    value1: that.circles.label + ': ' + data[that.circles.key],
                    value2: that.xAxis.label + ': ' + data[that.xAxis.key],
                    value3: that.yAxis.label + ': ' + data[that.yAxis.key],
                } );
            })
            .on('mouseleave', function(bar, index, rects) {
                that.tooltip.hide();
                let el = rects[index];
                el.removeAttribute('class', 'highlight');
            })
            .on('mouseenter', function(bar, index, rects) {
                that.tooltip.show();
                let el = rects[index];
                el.setAttribute('class', 'highlight');
            })
            .on('click', function(bar, index, rects) {
                let options = {data: bar, index, rects};
                that.triggerEvent('circleClicked', bar[that.circles.key]);
            })
    }

    deselectNeighbourhood(neighbourhood) {
        delete this.neighbourhoodColourMap[neighbourhood];
        this.updateData();
    }

    selectNeighbourhood(neighbourhood) {
        this.neighbourhoodColourMap[neighbourhood] = neighbourhood;
        this.updateData();
    }

    updateXAxis() {
        let axis = this.xAxis.d3Axis;
        let bottom = d3.axisBottom(this.x);
        if (this.xAxis.type === 'integer') {
            bottom.tickFormat(d3.format(".2s"));
        }

        axis.transition(this.transition)
            .call(bottom);
    }

    updateYAxis() {
        let axis = this.yAxis.d3Axis;
        let left = d3.axisLeft(this.y);
        if (this.yAxis.type === 'integer') {
            left.tickFormat(d3.format(".2s"));
        }
        else if (this.yAxis.type === 'string') {
            // TODO: If there is time, attempt to correctly format the text using a tickFormat function
//            left.tickFormat();
        }

        let that = this;
        axis.transition(this.transition)
            .on('end', function() {
                // Hackish way of limiting the text size.
                // Accounts for how much the label eats into the margin
                axis.selectAll('g text').each(function(value, i, dom) {
                    that._constrainAxisValueWithinLabel(this, 'y');
                });
            })
            .call(left);
    }

        // PRIVATE FUNCTIONS

    /*
        Purpose:
            Updates the x axis

        Returns:
            void
    */
    _initializeXAxis() {
        let bottom = d3.axisBottom(this.x);
        if (this.xAxis.type === 'integer') {
            bottom.tickFormat(d3.format(".2s"));
        }

        this.xAxis.d3Axis = this.svg
            .append('g')
            .attr('transform', `translate(0, ${this.height})`)
            .call(bottom);
    }

    /*
        Purpose:
            Updates the label on the x axis

        Returns:
            void
    */
    _initializeXLabel() {
        this.xAxis.d3Label = this.svg.append("text")
            .attr('transform', `translate(${this.width/2}, ${this.height + this.margin.bottom - 10})`)
            .style('text-anchor', 'middle')
            .text(this.xAxis.label)
    }

    /*
        Purpose:
            Updates the y axis

        Returns:
            void
    */
    _initializeYAxis() {
        let left = d3.axisLeft(this.y);
        if (this.yAxis.type === 'integer') {
            left.tickFormat(d3.format(".2s"));
        }

        let axis = this.yAxis.d3Axis = this.svg
            .append('g')
            .call(left);

        //
        let that = this;
        axis.selectAll('g text').each(function(value, i, dom) {
            that._constrainAxisValueWithinLabel(this, 'y');
        });
    }

    /*
        Purpose:
            Initializes the label on the y axis

        Returns:
            void
    */
    _initializeYLabel() {
        let that = this;
        this.yAxis.d3Label = this.svg.append("text")
            .each(function() {
                that.yLabelEl = this;
            })
            .attr('transform', `rotate(-90)`)
            .attr('x', 0 - this.height / 2)
            .attr('y', 0 - this.margin.left)
            .attr('dy', '1em')
            .style('text-anchor', 'middle')
            .text(this.yAxis.label)
    }

    /*
        Purpose:
            Constrains the string values beside the axis ticks, so that the value fits within the available space

        Returns:
            void
    */
    _constrainAxisValueWithinLabel(element, axisValue) {
        let clientText = element.getBoundingClientRect();
        let label = axisValue === 'y' ? this.yLabelEl : this.xLabelEl;
        let clientLabel = label.getBoundingClientRect();
        let textVal = element.innerHTML;
        let containerEl = element.parentElement.parentElement;
        let containerRect = containerEl.getBoundingClientRect();

        if (axisValue === 'y') {
//            let restriction = clientText.right - (clientLabel.left + clientLabel.width);
            let restriction =  containerRect.width - (clientLabel.left - containerRect.left + clientLabel.width) - 10;
            if (clientText.width > restriction) {

                // Calculate a rough estimation of how much smaller the text should be
                // the percent that the new width should be at
                let percent = restriction / clientText.width;

                // Calculate a rough estimation of the character limit
                // 3 is taken off from the char limit to compensate for '...'
                // add padding to the percentage
                let charLimit = Math.floor(textVal.length * (percent)) - 3;
                if (charLimit < textVal.length) {
                    textVal = textVal.substr(0, charLimit);
                    textVal = textVal.concat('...');

                    // Apply the text to the element
                    element.innerHTML = textVal;
                }
            }
        }
    }

    /*
        Purpose:
            Initializes the SVG element

        Returns:
            void
    */
    _initializeSvg(options) {
        this.graph = d3.select('#'+this.containerId).append('svg')
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)

        this.svg = d3.select('#'+this.containerId + ' svg g');
        let container = document.getElementById(this.containerId);
    }

    /*
        Purpose:
            Initializes and sets a lot of the important variables

        Returns:
            void
    */
    _setupVariables(options) {
        this.svg;
        this.graph;
        this.containerId = options.containerId || 'chart-container';
        this.dataContainer = options.data || this.dataContainer;
        this.data = this.dataContainer.rows;
        this.transition = d3.transition().duration(options.duration || 500);
        this.circles = options.circles || this.circles;
        this.neighbourhoodColourMap = this.neighbourhoodColourMap || {};

        // Maps the bar elements to the neighbourhood names
        this.barMap = {};

        this.margin = {
            top: options.margin && options.margin.top || 20,
            right: options.margin && options.margin.right || 5,
            bottom: options.margin && options.margin.bottom || 40,
            left: options.margin && options.margin.left || 80,
        };
        this.width = (options.width - this.margin.left - this.margin.right) || this.width;
        this.height = (options.height - this.margin.top - this.margin.bottom) || this.height;

        this.xAxis = options.xAxis || this.xAxis;
        this.xAxis.data = this.data.map((value) => { return value[this.xAxis.key] });

        this.yAxis = options.yAxis || this.yAxis;
        this.yAxis.data = this.data.map((value) => { return value[this.yAxis.key] });

        this.x = this._setXScale();
        this.y = this._setYScale();

        // Initialize tooltip
        if (!this.tooltip) {
            this.tooltip = new Tooltip({
                containerId: options.containerId,
            });
        }
    }

    /*
        Purpose:
            Gets the size that the bars should be at

        Returns:
            void
    */
    _getBarSize(a) {
        let isX = a === 'width';
        let axis = isX ? this.xAxis : this.yAxis;
        let v = isX ? this.x : this.y;
        let s = isX ? this.width : this.height;

        if (axis.type === 'category') {
            return v.bandwidth();
        }
        else {
            return (data) => {
                return v(data[axis.key]);
            }
        }
    }

    _setXScale() {
        return this._setScale(this.xAxis, this.width);
    }

    _setYScale() {
        return this._setScale(this.yAxis, this.height, 'max');
    }

    /*
        Parameters:
            axis: either the xAxis or yAxis
            size: either the width or the height

        Returns:
            returns the scale
    */
    _setScale(axis, size, firstVal) {
        let scale;
        let domain;
        if (axis.type == 'category') {
            domain = this.data.map( (value) => { return value[axis.key] } );
            scale = d3.scaleBand().padding(0.5);
        }
        else if (axis.type == 'integer') {
            let min = 0;
            let max = d3.max(this.data, (value) => { return value[axis.key] })
            domain = [min, max];
            if (firstVal === 'max') {
                domain = [max, min];
            }
            scale = d3.scaleLinear();
        }

        return scale.domain(domain).range([0, size])
    }
}