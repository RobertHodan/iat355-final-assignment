// References:
/*
    Simple line graph with v4
        https://gist.github.com/d3noob/402dd382a51a4f6eea487f9a35566de0
        - Looked at how they create a line graph

    Line Chart with Circle Tooltip D3 V4
        https://bl.ocks.org/alandunning/cfb7dcd7951826b9eacd54f0647f48d3
        - Looked at how they created hover lines

    Show just years for date
        https://stackoverflow.com/questions/32428122/what-scale-to-use-for-representing-years-only-on-x-axis-in-d3-js
*/
/*
    Class dependencies:
        Graph.js
        Tooltip.js
*/
/*
    Purpose:
        Creates a basic line graph, with a x and y axis and their respective labels.
*/
class LineGraph extends Graph {
    constructor(options) {
        super();

        this._setupVariables(options);
        this._initializeSvg();
        this._initializeXLabel();
        this._initializeXAxis();
        this._initializeYLabel();
        this._initializeYAxis();
        this.updateData();
        this.focus.attr('display', 'none');
    }

    addLine(newLine) {
        if (!newLine) return;

        if (!this.lines.values.includes(newLine)) {
            this.lines.values.push(newLine);
        } else {
            return;
        }

        this.setData({
            lines: this.lines,
        });
    }

    removeLine(newLine) {
        if (!newLine) return;

        if (!this.lines.values.includes(newLine)) {
            return;
        } else {
            let index = this.lines.values.indexOf(newLine);
            let line = this.lines.values.splice(index, 1);
            let colour = this.lines.colours.splice(index, 1);
        }

        this.setData({
            lines: this.lines,
        });
    }

    setData(options) {
        this._setupVariables(options);

        this.updateAll();
    }

    sortBy(string, value) {
        let func;
        if (string === 'greatest') {
            func = function(a, b) {
                return b[value] - a[value];
            }
        }
        else if (string === 'least') {
            func = function(a, b) {
                return a[value] - b[value];
            }
        }

        let options = {};
        options.data = this.dataContainer;
        options.data.rows = this.data.sort(func);
        this._setupVariables(options);
        this.updateAll();
    }

    updateAll() {
        this.updateData();
        this.updateXAxis();
        this.updateYAxis();
    }

    // Purpose:
    //      Updates the bars
    //
    // Parameters:
    //      data: array of values
    //
    // Returns:
    //      void
    updateData() {
        let data = this.data;

        /*
            Couldn't figure out how to use the update pattern with multi-line support
            So, just remove all paths every time, and re-add them all.
        */
        this.paths.selectAll('path').each(function() {
            this.remove();
        });

        // Select all the rects, within the svg object
        let paths = this.paths.selectAll('path')
            .data([data])

        // Discard data that is no longer used
//        paths.exit().remove();

        let that = this;
        for (let i=0; i < this.lines.values.length; i++) {
            let value = this.lines.values[i];
            let colour = window.legendColours[value];
            this.triggerEvent('lineAdded', {
                colour: colour,
                lineId: value,
            });

            let line = d3.line()
                .x((d) => {
                    return this.x( d[this.xAxis.key] )
                })
                .y((d) => {
                    if (d[value] === undefined) d[value] = 0;
                    return this.y(d[value]);
                });

            // Update the data
            paths.enter()
                .append('path')
                .data([data])
                .attr('class', 'line')
                .attr('d', line)
                .attr('stroke', colour)

            let li = document.createElement('li');
            let span = document.createElement('span');
            span.style.setProperty('background', colour);
            li.append(span);
            li.append(value);
            li.onclick = function() {
                that.toggleLine(value);
            };
        }

        this.paths.selectAll('path')
        .on('mousemove', function(bar, index, rects) {
            // Moves the focus to a hovered part of the graph
            // Copied this from a reference
            let mouseX = that.x.invert(d3.mouse(this)[0]);
            let bi = that.bisectDate(data, mouseX, 1);
            let d0 = data[bi - 1];
            let d1 = data[bi];
            let d = mouseX - d0[that.xAxis.key] > d1[that.xAxis.key] - mouseX  ? d1 : d0;
            let line = that.lines.values[index];
            let yPos = that.y(d[line]);

            that.focus.attr("transform", `translate( ${that.x(d[that.xAxis.key])}, ${yPos} )`);
            that.focus.select('line').attr("y2", that.height - yPos);

            // Brings the focus to the front / top layer
            that.focus.node().parentNode.appendChild(that.focus.node());
            that.focusData = {
                x: {
                    value: d[that.xAxis.key],
                },
                y: {
                    value: d[line],
                }
            };
        })
        .on('mouseenter', function(bar, index, rects) {
            that.focus.attr('display', 'inline-block');
        })
    }

    updateXAxis() {
        let axis = this.xAxis.d3Axis;
        let bottom = d3.axisBottom(this.x);
        if (this.xAxis.type === 'integer') {
            bottom.tickFormat(d3.format(".2s"));
        }
        else if (this.xAxis.type == 'date') {
            bottom.ticks( d3.timeYear.every(1) );
        }

        axis.transition(this.transition)
            .call(bottom);
    }

    updateYAxis() {
        let axis = this.yAxis.d3Axis;
        let left = d3.axisLeft(this.y);
        if (this.yAxis.type === 'integer') {
            left.tickFormat(d3.format(".2s"));
        }
        else if (this.yAxis.type === 'string') {
            // TODO: If there is time, attempt to correctly format the text using a tickFormat function
//            left.tickFormat();
        }

        let that = this;
        axis.transition(this.transition)
            .call(left);
    }

        // PRIVATE FUNCTIONS

    /*
        Purpose:
            Updates the x axis

        Returns:
            void
    */
    _initializeXAxis() {
        let bottom = d3.axisBottom(this.x);
        if (this.xAxis.type === 'integer') {
            bottom.tickFormat(d3.format(".2s"));
        }
        else if (this.xAxis.type == 'date') {
            bottom.ticks( d3.timeYear.every(1) );
        }

        this.xAxis.d3Axis = this.svg
            .append('g')
            .attr('transform', `translate(0, ${this.height})`)
            .call(bottom);
    }

    /*
        Purpose:
            Updates the label on the x axis

        Returns:
            void
    */
    _initializeXLabel() {
        this.xAxis.d3Label = this.svg.append("text")
            .attr('transform', `translate(${this.width/2}, ${this.height + this.margin.bottom - 10})`)
            .style('text-anchor', 'middle')
            .text(this.xAxis.label)
    }

    /*
        Purpose:
            Updates the y axis

        Returns:
            void
    */
    _initializeYAxis() {
        let left = d3.axisLeft(this.y);
        if (this.yAxis.type === 'integer') {
            left.tickFormat(d3.format(".2s"));
        }

        let axis = this.yAxis.d3Axis = this.svg
            .append('g')
            .call(left);
    }

    /*
        Purpose:
            Initializes the label on the y axis

        Returns:
            void
    */
    _initializeYLabel() {
        let that = this;
        this.yAxis.d3Label = this.svg.append("text")
            .each(function() {
                that.yLabelEl = this;
            })
            .attr('transform', `rotate(-90)`)
            .attr('x', 0 - this.height / 2)
            .attr('y', 0 - this.margin.left)
            .attr('dy', '1em')
            .style('text-anchor', 'middle')
            .text(this.yAxis.label)
    }

    /*
        Purpose:
            Initializes the SVG element

        Returns:
            void
    */
    _initializeSvg(options) {
        this.graph = d3.select('#'+this.containerId).append('svg')
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)

        this.svg = d3.select('#'+this.containerId + ' svg g');
        this.focus = this.svg.append('g')
            .attr('class', 'focus')
            .attr('transform', `translate(50, 50)`)
//            .style('display', 'none')

        this.focus.append("line")
        .attr("class", "hover-line")
        .attr("y1", 0)
        .attr("y2", this.height - this.margin.bottom);

//        this.focus.append('line')
//            .attr('class', 'y-hover-line hover-line')
//            .attr('x1', this.width)
//            .attr('x2', this.width)

        let that = this;
        this.focusData = {};
        this.focus.append('circle')
            .attr('r', 7.5)
            .on('mousemove', function(bar, index, rects) {
                let circleRect = this.getBoundingClientRect();
                let bodyRect = document.body.getBoundingClientRect();
                let coords = [circleRect.x, circleRect.y];
                that.tooltip.moveTo(coords[0] + 20,
                                    coords[1] - bodyRect.top + 40);
                let format = d3.format(",");
//                let number = format(bar[that.xAxis.key]);
                that.tooltip.setData( {
                    value1: that.xAxis.label + ': ' + that.focusData.x.value.getFullYear(),
                    value2: that.yAxis.label + ': ' + that.focusData.y.value,
                } );
            })
            .on('mouseleave', function(bar, index, rects) {
                that.tooltip.hide();
                let el = rects[index];
                el.removeAttribute('class', 'highlight');
            })
            .on('mouseenter', function(bar, index, rects) {
                that.tooltip.show();
                let el = rects[index];
                el.setAttribute('class', 'highlight');
            })

        this.paths = this.svg.append('g');
        let container = document.getElementById(this.containerId);
    }

    /*
        Purpose:
            Creates new data that's more compatible with the line graph:

        Returns:
            void
    */
    _createNewData(original, lines) {
        let newData = [];

        let axisOther = this.yAxis.type === 'date' ? this.xAxis : this.yAxis;
        let axisDate = this.yAxis.type === 'date' ? this.yAxis : this.xAxis;
        this.axisDate = axisDate;
        this.axisOther = axisOther;

        let yearIndex = {};
        for (let i=0; i<original.rows.length; i++) {
            let row = original.rows[i];
            let date = row[axisDate.key];

            let newRow;
            if (yearIndex[date] === undefined) {
                yearIndex[date] = newData.length;
                newRow = {};
                newRow[axisDate.key] = row[axisDate.key];
                newData.push(newRow);
            }
            else {
                newRow = newData[yearIndex[date]];
            }

            if (lines.values.includes(row[lines.key])) {
                let lineVal = lines.values[ lines.values.indexOf(row[lines.key]) ];
                if (row[lines.key] === lineVal)  {
                    newRow[lineVal] = row[axisOther.key];
                }
            }
        }

        return newData;
    }

    /*
        Purpose:
            Initializes and sets a lot of the important variables

        Returns:
            void
    */
    _setupVariables(options) {
        this.containerId = options.containerId || this.containerId || 'chart-container';
        this.el = document.getElementById(this.containerId);

        this.graph;
        this.originalData = options.data || this.originalData;
        this.lines = options.lines || this.lines;
        if (!this.lines.values) this.lines.values = [];
        if (!this.lines.colours) this.lines.colours = [];

        this.transition = d3.transition().duration(options.duration || 500);

        this.margin = {
            top: options.margin && options.margin.top || 20,
            right: options.margin && options.margin.right || 5,
            bottom: options.margin && options.margin.bottom || 40,
            left: options.margin && options.margin.left || 80,
        };
        this.width = (options.width - this.margin.left - this.margin.right) || this.width;
        this.height = (options.height - this.margin.top - this.margin.bottom) || this.height;

        this.xAxis = options.xAxis || this.xAxis;

        this.yAxis = options.yAxis || this.yAxis;

        this.data = this._createNewData(this.originalData, this.lines);
        this.xAxis.data = this.data.map((value) => { return value[this.xAxis.key] });
        this.yAxis.data = this.data.map((value) => { return value[this.yAxis.key] });
        let date = this.yAxis.type === 'date' ? this.yAxis.key : this.xAxis.key;
        this.bisectDate = d3.bisector(function(d) { return d[date] }).left;

        this.x = this._setXScale();
        this.y = this._setYScale();

        // Initialize tooltip
        if (!this.tooltip) {
            this.tooltip = new Tooltip({
                containerId: options.containerId,
            });
        }
    }

    _setXScale() {
        return this._setScale(this.xAxis, this.width);
    }

    _setYScale() {
        return this._setScale(this.yAxis, this.height, 'max');
    }

    /*
        Parameters:
            axis: either the xAxis or yAxis
            size: either the width or the height

        Returns:
            returns the scale
    */
    _setScale(axis, size, firstVal) {
        let scale;
        let domain;
        if (axis.type == 'category') {
            domain = this.data.map( (value) => { return value[axis.key] } );
            scale = d3.scaleBand().padding(0.5);
        }
        else if (axis.type == 'integer') {
            let min = 0;
            // Get the maximum value out of all the lines
            let max = d3.max(this.data, (value) => {
                let array = [];
                for (let i=0; i < this.lines.values.length; i++) {
                    array.push( value[this.lines.values[i]] );
                }

                return d3.max(array);
            });
            if (firstVal === 'max') {
                domain = [max, min];
            } else {
                domain = [min, max];
            }
            scale = d3.scaleLinear();
        }
        else if (axis.type == 'date') {
            domain = d3.extent(this.data, (value) => { return value[axis.key] });
            scale = d3.scaleTime();
        }

        return scale.domain(domain).range([0, size]);
    }
}