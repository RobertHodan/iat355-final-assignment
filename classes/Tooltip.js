// A class to more easily move around an existing tooltip element
class Tooltip {
    // Parameters:
    //      containerId: id of the tooltip
    constructor(options) {
        this.containerId = options.containerId;
        this.el = this._getTooltipElement(options.containerId);
        this.hide();
    }

    // Sets the data of this tooltip
    setData(object) {
        // If we need different types of tooltips, then we need to change this
        let v1 = this.el.getElementsByClassName('value1')[0];
        let v2 = this.el.getElementsByClassName('value2')[0];
        let v3 = this.el.getElementsByClassName('value3')[0];

        v1.innerHTML = object.value1;
        v2.innerHTML = object.value2;
        if (v3) v3.innerHTML = object.value3;
    }

    // Makes the tooltip element invisible
    hide() {
        this.el.style.setProperty('display', 'none');
    }

    // Parameters:
    //      x
    //      y
    moveTo(x, y) {
        let client = this.el.getBoundingClientRect();
        this.el.style.setProperty('top', y + 'px');
        this.el.style.setProperty('left', x + 'px');
    }

    // Makes the tooltip element visible
    show() {
        this.el.style.setProperty('display', 'block');
    }

    // Private

    _getTooltipElement(id) {
        let container = document.getElementById(id);
        if (!container) return null;

        return container.getElementsByClassName('tooltip')[0];
    }

    // Unused
    _appendTo(id) {
        let container = document.getElementById(id);
        this.el = this._createTemplate();

        container.append(this.el);
    }

    _createTemplate() {
        let template = document.createElement('div');
        template.setAttribute('class', 'tooltip');

        return template;
    }
}