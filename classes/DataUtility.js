// In charge of holding functions that calculate or return new data
class DataUtility {

    constructor() {}

//    datasets: [allNeighbourhoodsCrime, allNeighbourhoodsRacks],
//    group: {
//        values: ['neighbourhood'],
//        keys: [['NEIGHBOURHOOD', 'Neighbourhood']]
//    },
//    output: {
//        stolenBikes: [0, 'count'],
//        installedRacks: [1, 'count'],
//        neighbourhood: 'group',
//    },

    combine(options) {
        let newRows = [];
        let groupToRowIndex = {};
        let datasets = options.datasets;
        for (let i=0; i < datasets.length; i++) {
            let dataset = datasets[i].rows;

            for (let j=0; j<dataset.length; j++) {
                let row = dataset[j];

                let groupKey = "";
                // Create a group key
                let datasetKey;
                for (let k=0; k<options.group.values.length; k++) {
                    let key = options.group.values[k];
                    datasetKey = options.group.keys[k][i];
                    let value = row[datasetKey];
                    groupKey += value;
                }

                // Check if the group key exists, if so get the row. Otherwise, make a row
                let rowIndex = groupToRowIndex[groupKey];
                let newRow;
                if (rowIndex === undefined) {
                    newRow = [];
                    rowIndex = newRows.push(newRow)-1; // returns length, so minus 1
                    groupToRowIndex[groupKey] = rowIndex;
                }
                else {
                    newRow = newRows[rowIndex];
                }

                let keys = Object.keys(options.output);
                let output = options.output;
                for(let k=0; k<keys.length; k++) {
                    let outputKey = keys[k];
                    let outputValue = output[outputKey];

                    let datasetIndex = outputValue[0];
                    let datasetKey = outputValue[1];
                    if (datasetIndex === 'group') {
                        datasetIndex = i;
                        datasetKey = options.group.keys[outputValue[1]][i];
                    }

                    if (datasetIndex === i) {
                        newRow[outputKey] =  row[datasetKey];
                    }
                }
            }
        }

        return {
            excluded: {},
            rows: newRows,
        }
    };

    // About
//      Returns all records matching a specified criteria.
//      If a given value is not the same as the specified value, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
        // Parameters
//        scope:
//            key: the field name, eg. 'Distributor'
//            value: the value to scope to, eg. '20th Century Fox'
//        filter:
//            type: the type of filter to be applied. eg. 'greaterThan'
//            key: the key to search within, eg. 'Worldwide_Gross'
//            value: the value to be used with the filter type. eg. 20
//        dataId: the id of the data object to search within, eg. 'movies'
//
    // Returns
//      value: the number of records counted
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getRecords(options) {
        let data = this.getData(options.dataId);
        // Initialize filter as an empty object if it doesn't exist (error prevention)
        if (options.filter === undefined) options.filter = {};
        if (options.scope === undefined) options.scope = {};

        let row = {};
        let array = [];
        let excluded = {};

        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope === 'undefined' || row[options.scope.key] === options.scope.value) {
                // The value searched for must match the variable type of the filterValue
                let variableType = typeof(options.filter.value);
                if ( variableType &&
                     typeof(row[options.filter.key]) != variableType ) {
                    // eg. If the type is a string, 'NotAString' gets passed as the second parameter
                    this._addExcludedCount(excluded, 'NotA' + variableType.charAt(0).toUpperCase() + variableType.slice(1), row);

                    // Skip this row
                    continue;
                }

                // Operations
                if (options.filter.type === 'greaterThan' &&
                    row[options.filter.key] > options.filter.value) {
                    array.push(row);
                }
                else if (options.filter.type === 'smallerThan' &&
                    row[options.filter.key] < options.filter.value) {
                    array.push(row);
                }
                else if (options.filter.type === 'equalTo' &&
                    row[options.filter.key] === options.filter.value) {
                    array.push(row);
                }
                else if (options.filter.type === 'includes') {
                    let lower = row[options.filter.key].toLowerCase();
                    if (lower.includes(options.filter.value.toLowerCase())) {
                        array.push(row);
                    }
                }

                if (options.filter.key === undefined) {
                    array.push(row);
                }
            }
        }

        return {
            rows: array,
            excluded: excluded,
        }
    }

    // About
//      Returns the number of records matching a specified criteria.
//      If a given value is not the same as the specified value, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
        // Parameters
//        group:
//          value: the field name that the data should be grouped by
//          exclude: array with values to exclude
//        scope:
//            key: the field name, eg. 'Distributor'
//            value: the value to scope to, eg. '20th Century Fox'
//        filter:
//            type: the type of filter to be applied. eg. 'greaterThan'
//            key: the key to search within, eg. 'Worldwide_Gross'
//            value: the value to be used with the filter type. eg. 20
//        countBy: the key that should be added to the count
//        dataId: the id of the data object to search within, eg. 'movies'
//
    // Returns
//      value: the number of records counted
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getCountByGroups(options) {
        let data = this.getData(options.dataId);
        // Initialize filter as an empty object if it doesn't exist (error prevention)
        if (options.filter === undefined) options.filter = {};
        if (options.scope === undefined) options.scope = {};

        let groups = {};
        let row = {};
        let rows = [];
        let newRows = [];
        let excluded = {};
        // Map the group name to its respective index number for newRows
        let groupIndex = {};

        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (this._withinScope(options.scope, row)) {
                if (this._withinFilter(options.filter, row)) {
                    this._incrementGroupCount(options.group, groups, groupIndex, row, newRows, excluded, options.countBy);
                }
            }
        }

        return {
            rows: newRows,
            excluded: excluded,
        }
    }

    // About
//      Returns the max value of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the max of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated max
//      info: the information of the row that the maximum was taken from
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getMax(options) {
        let data = this.getData(options.dataId);
        // Initialize scope as an empty object if it doesn't exist (error prevention)
        if (options.scope === undefined) options.scope = {};

        let row = {};
        let max = null;
        let maxInfo = null;
        let excluded = {};

        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the max
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                // If max is not set or if it's lower, then set it to the current value
                if (max === null || max < row[options.valueOf]) {
                    max = row[options.valueOf];
                    maxInfo = row;
                }
            }
        }

        return {
            value: max,
            info: maxInfo,
            excluded: excluded,
        }
    }

    // About
//      Returns the minimum value of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the min of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated min
//      info: the information of the row that the minimum was taken from
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getMin(options) {
        let data = this.getData(options.dataId);
        if (options.scope === undefined) options.scope = {};

        let row = {};
        let min = null;
        let minInfo = null;
        let excluded = {};
        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the min
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                if ( row[options.valueOf] === options.exclude ) {
                    this._addExcludedCount(excluded, 'ValueExcluded');

                    // Skip this row
                    continue;
                }

                // If min is not set or if it's lower, then set it to the current value
                if (min === null || min > row[options.valueOf]) {
                    min = row[options.valueOf];
                    minInfo = row;
                }
            }
        }

        return {
            value: min,
            info: minInfo,
            excluded: excluded,
        }
    }

    // About
//      Returns the sum of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the sum of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated sum
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getSum(options) {
        let data = this.getData(options.dataId);
        if (options.scope === undefined) options.scope = {};
        let sumValue = 0;
        let row = {};
        let excluded = {};
        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the min
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                if ( row[options.valueOf] === options.exclude ) {
                    this._addExcludedCount(excluded, 'ValueExcluded');

                    // Skip this row
                    continue;
                }

                sumValue = sumValue + row[options.valueOf];

            }
        }

        return {
            value: sumValue,
            excluded: excluded,
        }
    }

    // About
//      Returns the average value of a set of records.
//      If a given value is not a number, then it’s not counted.
//      However, we still keep track of it, and it gets returned under the “excluded” object.
//
    // Parameters
//        scope:
//          key: the field name, eg. 'Distributor'
//          value: the value to scope to, eg. '20th Century Fox'
//        valueOf: the value to search the average of, eg. 'Worldwide_Gross'
//        dataId: the id of the data to search within, eg. 'movies'
//
    // Returns
//      value: the calculated average
//      excluded: values that are within the scope/filter, but were not included in the calculation
    getAverage(options) {
        let data = this.getData(options.dataId);
        if (options.scope === undefined) options.scope = {};
        let count = 0;
        let sumValue = 0;
        let average = 0;
        let row = {};
        let excluded = {};
        for (let i=0; i < data.length; i++) {
            row = data[i];

            // Check if we're limiting the search based on a value
            if (options.scope.key === 'undefined' || row[options.scope.key] === options.scope.value) {
                // Check if value is a number, otherwise we can't determine the min
                if ( typeof(row[options.valueOf]) != 'number' ) {
                    this._addExcludedCount(excluded, 'NotANumber');

                    // Skip this row
                    continue;
                }

                if ( row[options.valueOf] === options.exclude ) {
                    this._addExcludedCount(excluded, 'ValueExcluded');

                    // Skip this row
                    continue;
                }

                sumValue = sumValue + row[options.valueOf];
                count++;
            }
        }
 
        average = sumValue/count; 
 
        return {
            value: average,
            excluded: excluded,
        }
    }

    // PRIVATE FUNCTIONS

    // Mainly added for the purposes of seeing how many values are excluded
    _addExcludedCount(excluded, value, row) {
        if (!excluded[value]) {
            excluded[value] = [];
        }

        excluded[value].push(row);
    }

    // Excludes values if they match the row
    //  parameters:
    //      fieldName: field of row that should be checked
    //      exclude: array of values that should be checked against
    //      row: the row of data
    //      excluded: object that holds all excluded values
    _excludeRows( fieldNames, exclude, excluded, row) {
        let isExcluded = false;
        for (let i=0; i<fieldNames.length; i++) {
            let fieldValue = row[fieldNames[i]];
            if (fieldValue && fieldValue.constructor.name === 'Date') fieldValue = fieldValue.toString();

            if (!exclude) return isExcluded;
            for (let j=0; j<exclude.length; j++) {
                let excludeValue = exclude[j];
                if (excludeValue && excludeValue.constructor.name === 'Date') excludeValue = excludeValue.toString()

                if (fieldValue === excludeValue) {
                    this._addExcludedCount(excluded, excludeValue, row);
                    isExcluded = true;
                }
            }
        }
        return isExcluded;
    }

    _incrementGroupCount(group, groups, groupIndex, row, newRows, excluded, countBy) {
        let isExcluded = this._excludeRows(group.values, group.exclude, excluded, row);
        if (isExcluded) return;

        // Make a rowId, to easily manage grouping of multiple values
        let rowId = "";
        for (let i=0; i < group.values.length; i++) {
            let groupKey = group.values[i];
            let groupName = row[groupKey];
            rowId += groupName;
        }

        let groupName = row[group.value];
        if (!groups[rowId]) {
            groups[rowId] = {};
        }

        let countValue = parseInt(row[countBy]) || 1;
        if (groupIndex[rowId] === undefined) {
            let newRow = {count: countValue};

            // Add the values that are being grouped by as tables
            for (let i=0; i < group.values.length; i++) {
                let groupKey = group.values[i];
                let groupName = row[groupKey];
                newRow[groupKey] = groupName;
            }

            groupIndex[rowId] = newRows.length;
            newRows.push(newRow);
        } else {
            let index = groupIndex[rowId];
            newRows[index].count += countValue;
        }
    }

    _withinFilter(filter, row) {
        if (Array.isArray(filter)) return this._withinFilters(filter, row);
        if (filter.value && filter.value.constructor.name === 'Date') filter.value = filter.getFullYear();
        let rowValue = row[filter.key];
        if (rowValue && rowValue.constructor.name === 'Date') rowValue = rowValue.getFullYear();

        if (filter.key === undefined) {
            return true;
        }

        if (filter.type === 'greaterThan' &&
            rowValue > filter.value) {
            return true;
        }

        if (filter.type === 'lessThan' &&
            rowValue < filter.value) {
            return true;
        }

        if (filter.type === 'equalTo' &&
            rowValue === filter.value) {
            return true;
        }

        if (filter.type === 'includes') {
            let lower = row[filter.key].toLowerCase();
            if (lower.includes(filter.value.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    _withinFilters(filters, row) {
        // If any of the filters are false, then return false
        for (let i=0; i < filters.length; i++) {
            let value = this._withinFilter(filters[i], row);
            if (value === false) return false;
        }

        return true;
    }

    // Checks if the row fits within the contraint / scope
    // parameters:
    //      scope:
    //          key: the key whose value should be restricted
    //          value: the value that can be accepted
    //      row: a row of the given data
    _withinScope(scope, row) {
        if (scope === 'undefined') return true;

        if (row[scope.key] === scope.value) return true;

        // Check if we're scoping the date to a given range
        // First check if the constructor's name is 'Date', and assume it is D3's Date object
        // Not the best solution, but it's good enough for this project
        if (row[scope.key].constructor.name === 'Date') {
            // The format is assumed to be "Wed Jan 01 2014"
            let dateString = row[scope.key].toDateString().toLowerCase();
            let value = scope.value.toString().toLowerCase();

            if (dateString.includes(value)) return true;
        }

        return false;
    }
}