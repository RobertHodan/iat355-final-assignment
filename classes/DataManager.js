// In charge of holding and managing data
//
/*
    Dependencies:
        DataUtility.js
        D3.js
*/
class DataManager extends DataUtility {
    constructor() {
        super();
        this.data = {};
    }

    addData(data, dataId) {
        this.data[dataId] = data;
    }

    getData(dataId) {
        return this.data[dataId];
    }

    hasData(array) {
        let hasData = true;
        for (let i=0; i<array.length; i++) {
            let key = array[i];
            if (!this.data[key]) hasData = false;
        }
        return hasData;
    }

    parseTime(dataId, fieldId, parseValue) {
        let data = this.data[dataId];

        let parseTime = d3.timeParse('%Y');
        for (let i=0; i < data.length; i++) {
            let field = data[i][fieldId];

            data[i][fieldId] = parseTime(field);
        }

        return parseTime;
    }
}