// Class for managing the legend
class Legend {
    constructor(options) {
        this.subscriptions = {};
        this.containerId = options.containerId;
        this.containerEl = document.getElementById(this.containerId);
        this.labelsContainerEl = window.labels = this.containerEl.getElementsByClassName('legend-labels')[0];
        this.labels = {};
    }

    addLabel(label) {
        let labelEl = this.labels[label];

        if (labelEl === undefined) {
            let li = document.createElement('li');
            let span = document.createElement('span');
            let colour = window.legendColours[label];

            span.style.setProperty('background', colour);
            li.append(span);
            li.append(label);

            this.labels[label] = li;
            this.labelsContainerEl.append(li);

            li.onclick = () => {
                this.toggleLabel(label);
            }

            this.triggerEvent('legendLabelAdded', label);
        }
    }

    removeLabel(label) {
        let labelEl = this.labels[label];

        if (labelEl !== undefined) {
            this.labelsContainerEl.removeChild(labelEl);
            delete this.labels[label];
            this.triggerEvent('legendLabelRemoved', label);
        }
    }

    toggleLabel(label) {
        let labelEl = this.labels[label];

        if (labelEl === undefined) {
            this.addLabel(label);
        } else {
            this.removeLabel(label);
        }
    }

    // Subscribe to an event
    subscribe(eventId, callback) {
        if (this.subscriptions[eventId] === undefined) this.subscriptions[eventId] = [];

        this.subscriptions[eventId].push(callback);
    }

    // Trigger an event, so that subscriptions can know
    triggerEvent(eventId, options) {
        let callbacks = this.subscriptions[eventId];

        if (callbacks && callbacks.length > 0) {
            for (let i=0; i < callbacks.length; i++) {
                callbacks[i](options);
            }
        }
    }
}