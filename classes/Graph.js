// Basic graph class
// If there's time, would be nice to put common functionality in here
class Graph {
    constructor() {
        this.subscriptions = {};
    }

    // Subscribe to an event
    subscribe(eventId, callback) {
        if (this.subscriptions[eventId] === undefined) this.subscriptions[eventId] = [];

        this.subscriptions[eventId].push(callback);
    }

    // Trigger an event, so that subscriptions can know
    triggerEvent(eventId, options) {
        let callbacks = this.subscriptions[eventId];

        if (callbacks && callbacks.length > 0) {
            for (let i=0; i < callbacks.length; i++) {
                callbacks[i](options);
            }
        }
    }
}