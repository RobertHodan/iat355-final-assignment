function main() {
    let dataManager = new DataManager();

    // Debugging purposes
    this.dataManager = dataManager;
    let dataSets = ['crime', 'bikeRacks'];

    let dataFileCrime = 'crime_csv_all_years_2.csv';
    let dataUrlCrime = "data/" + dataFileCrime;
    if (window.location.origin === "file://") {
        dataUrlCrime = 'http://www.sfu.ca/~rhodan/iat355/' + dataFileCrime;
    }

    d3.csv(dataUrlCrime, function(data) {
        dataManager.addData(data, dataSets[0]);

        checkLoadedData(dataManager, dataSets);
    });

    let dataFileBikeRacks = 'BikeRackData.csv';
    let dataUrlBikeRacks = "data/" + dataFileBikeRacks;
    if (window.location.origin === "file://") {
        dataUrlBikeRacks = 'http://www.sfu.ca/~rhodan/iat355/' + dataFileBikeRacks;
    }
    d3.csv(dataUrlBikeRacks, function(data) {
        dataManager.addData(data, dataSets[1]);

        checkLoadedData(dataManager, dataSets);
    });

    // Quick hackish solution. Instead of properly passing it around, just keep it in the global scope
    window.legendColours = {
        'Grandview-Woodland': '#a73544',
        'West End': '#9554cd',
        'Mount Pleasant': '#dfb169',
        'Stanley Park': '#27c53a',
        'Central Business District': '#292216',
        'Fairview': '#290016',
        'West Point Grey': '#abb99f',
        'Strathcona': '#BE6ADA',
        'Dunbar-Southlands': '#BEBADA',
        'Victoria-Fraserview': '#123472',
        'Sunset': '#DDB1D3',
        'Kensington-Cedar Cottage': '#99D3C7',
        'Riley Park': '#FFFF13',
        'Renfrew-Collingwood': '#F88F13',
        'Hastings-Sunrise': '#BEB002',
        'Killarney': '#A1072C',
        'Kerrisdale': '#FFCCD3',
        'Kitsilano': '#00D3C7',
        'Oakridge': '#00AA00',
        'South Cambie': '#FF00B3',
        'Shaughnessy': '#CCBADA',
        'Arbutus Ridge': '#6FF0AB',
        'Marpole': '#80B1C3',
        'Kitsilano': '#07D3C7',
        'Musqueam': '#A064E3',
    }
}

function checkLoadedData(dataManager, dataSets) {
    if (dataManager.hasData(dataSets)) {
        dataLoaded(dataManager);
    }
}

// Perform tasks after the json file was loaded
function dataLoaded(dataManager) {
    // More uglyness, for the sake of shortcuts
    let parse = window.parse = dataManager.parseTime('crime', 'YEAR', '%Y');
    dataManager.parseTime('bikeRacks', 'Year Installed', '%Y');

    let allNeighbourhoodsCrime = dataManager.getCountByGroups({
        group: {
            values: ['NEIGHBOURHOOD'],
            exclude: [""],
        },
        filter: {
            key: 'TYPE',
            type: 'includes',
            value: 'Bicycle',
        },
        dataId: 'crime',
    });

    let allNeighbourhoodsRacks = dataManager.getCountByGroups({
        group: {
            values: ['Neighbourhood'],
        },
        countBy: '# of racks',
        dataId: 'bikeRacks',
    })

    let allYearsCrime = dataManager.getCountByGroups({
        group: {
            values: ['NEIGHBOURHOOD', 'YEAR'],
            exclude: ["", parse('2018')],
        },
        filter: {
            key: 'TYPE',
            type: 'includes',
            value: 'Bicycle',
        },
        dataId: 'crime',
    });

    let allYearsBikeRacks = dataManager.getCountByGroups({
        group: {
            values: ['Neighbourhood', 'Year Installed'],
        },
        filter: [{
            key: 'Year Installed',
            type: 'greaterThan',
            value: 2013,
        },
        {
            key: 'Year Installed',
            type: 'lessThan',
            value: 2018,
        }],
        countBy: '# of racks',
        countLabel: 'numOfRacks',
        dataId: 'bikeRacks',
    });

    let allNeighbourhoods = dataManager.combine({
        datasets: [allNeighbourhoodsCrime, allNeighbourhoodsRacks],
        group: {
            values: ['neighbourhood'],
            keys: [['NEIGHBOURHOOD', 'Neighbourhood']],
        },
        output: {
            stolenBikes: [0, 'count'],
            installedRacks: [1, 'count'],
            neighbourhood: ['group', 0],
        },
    });

    let legend = window.legend = new Legend({
        containerId: 'legend',
    });

    let barGraphCrime = window.barGraphCrime = new BarGraph({
        data: allNeighbourhoodsCrime,
        yAxis: {
            key: 'NEIGHBOURHOOD',
            label: 'Neighbourhood',
            type: 'category',
        },
        xAxis: {
            key: 'count',
            label: '# of Bicycle thefts committed',
            type: 'integer',
        },
        width: 500,
        height: 500,
        margin: {
            top: 0,
            right: 20,
            bottom: 50,
            left: 120,
        },
        containerId: 'bar-chart-1',
    });

    let barGraphBike = window.barGraphBike = new BarGraph({
        data: allNeighbourhoodsRacks,
        yAxis: {
            key: 'Neighbourhood',
            label: 'Neighbourhood',
            type: 'category',
        },
        xAxis: {
            key: 'count',
            label: '# of Bicycle racks',
            type: 'integer',
        },
        width: 500,
        height: 500,
        margin: {
            top: 0,
            right: 20,
            bottom: 50,
            left: 120,
        },
        containerId: 'bar-chart-2',
    });

    let lineGraphCrime = window.lineGraphCrime = new LineGraph({
        data: allYearsCrime,
        yAxis: {
            key: 'count',
            label: '# of Bicycle thefts committed',
            type: 'integer',
        },
        xAxis: {
            key: 'YEAR',
            label: 'Year',
            type: 'date',
        },
        lines: {
            key: 'NEIGHBOURHOOD',
        },
        width: 400,
        height: 300,
        margin: {
            top: 0,
            right: 20,
            bottom: 50,
            left: 60,
        },
        containerId: 'line-chart-1',
    });

    let lineGraphBikeRacks = new LineGraph({
        data: allYearsBikeRacks,
        yAxis: {
            key: 'count',
            label: '# of Bike Racks installed',
            type: 'integer',
        },
        xAxis: {
            key: 'Year Installed',
            label: 'Year Installed',
            type: 'date',
        },
        lines: {
            key: 'Neighbourhood',
        },
        width: 400,
        height: 300,
        margin: {
            top: 0,
            right: 20,
            bottom: 50,
            left: 60,
        },
        containerId: 'line-chart-2',
    });

    let scatterplot = new ScatterplotGraph({
        data: allNeighbourhoods,
        yAxis: {
            key: 'stolenBikes',
            label: '# of stolen bikes',
            type: 'integer',
        },
        xAxis: {
            key: 'installedRacks',
            label: '# of bike racks installed',
            type: 'integer',
        },
        circles: {
            key: 'neighbourhood',
            label: 'Neighbourhood',
        },
        width: 800,
        height: 500,
        margin: {
            top: 0,
            right: 20,
            bottom: 50,
            left: 80,
        },
        containerId: 'scatterplot',
    });

    // Setup events
    barGraphCrime.subscribe('barClicked', legend.toggleLabel.bind(legend));
    barGraphBike.subscribe('barClicked', legend.toggleLabel.bind(legend));
    scatterplot.subscribe('circleClicked', legend.toggleLabel.bind(legend));

    legend.subscribe('legendLabelAdded', barGraphCrime.selectNeighbourhood.bind(barGraphCrime));
    legend.subscribe('legendLabelRemoved', barGraphCrime.deselectNeighbourhood.bind(barGraphCrime));
    legend.subscribe('legendLabelAdded', barGraphBike.selectNeighbourhood.bind(barGraphBike));
    legend.subscribe('legendLabelRemoved', barGraphBike.deselectNeighbourhood.bind(barGraphBike));

    legend.subscribe('legendLabelAdded', lineGraphCrime.addLine.bind(lineGraphCrime));
    legend.subscribe('legendLabelRemoved', lineGraphCrime.removeLine.bind(lineGraphCrime));
    legend.subscribe('legendLabelAdded', lineGraphBikeRacks.addLine.bind(lineGraphBikeRacks));
    legend.subscribe('legendLabelRemoved', lineGraphBikeRacks.removeLine.bind(lineGraphBikeRacks));

    legend.subscribe('legendLabelAdded', scatterplot.selectNeighbourhood.bind(scatterplot));
    legend.subscribe('legendLabelRemoved', scatterplot.deselectNeighbourhood.bind(scatterplot));

    // Add default labels
//    legend.toggleLabel('West End');
//    legend.toggleLabel('Central Business District');

    // Default sorting
    barGraphCrime.sortBy('count', 'desc');
    barGraphBike.sortBy('count', 'desc');

    // Connect buttons
    let buttonsContainer = document.getElementById('buttons-container');
    let buttons = buttonsContainer.getElementsByTagName('button');
    let type = 'number';
    let order = 'asc';
    buttons[0].addEventListener('click', function() {
        type = 'alphabetical';
        sort(type, order);
    });
    buttons[1].addEventListener('click', function() {
        type = 'number';
        sort(type, order);
    });
    buttons[2].addEventListener('click', function() {
        order = 'asc';
        sort(type, order);
    });
    buttons[3].addEventListener('click', function() {
        order = 'desc';
        sort(type, order);
    });

    let button = document.getElementById('selectAll');
    button.addEventListener('click', function() {
        let keys = Object.keys(window.legendColours);
        for (let i=0; i<keys.length; i++) {
            let key = keys[i];
            legend.addLabel(key);
        }
    });

    button = document.getElementById('deselectAll');
    button.addEventListener('click', function() {
        let keys = Object.keys(window.legendColours);
        for (let i=0; i<keys.length; i++) {
            let key = keys[i];
            legend.removeLabel(key);
        }
    });

    function sort(type, order) {
        if (type === 'alphabetical') {
            barGraphCrime.sortBy('NEIGHBOURHOOD', order);
            barGraphBike.sortBy('Neighbourhood', order);
        }
        else if (type === 'number') {
            barGraphCrime.sortBy('count', order);
            barGraphBike.sortBy('count', order);
        }
    }
}

function parseBikeData(dataManager) {
    let data = dataManager.getData('bikeRacks');

    let streets = {};
    for (let i=0; i < data.length; i++) {
        let row = data[i];
        let streetName = row['Street Name'];
        let neighbourhoods = dataManager.getCountByGroups({
            group: {
                values: ['NEIGHBOURHOOD'],
            },
            filter: {
                key: 'HUNDRED_BLOCK',
                type: 'includes',
                value: streetName,
            },
            dataId: 'crime',
        });

        let newKey = 'Neighbourhood';
        if (!streets[streetName]) {
            let newValue;
            console.log('Rows:')
            neighbourhoods.rows.forEach(function(row){
                console.log('    ' + row.NEIGHBOURHOOD + ' : ' + row.count);
            });
            console.log('Street Name: ' + streetName);
            console.log('\n\n\n');
            data[i][newKey] = newValue;
            streets[streetName] = newValue;
        }
        else {
            let newValue = streets[streetName];
            data[i][newKey] = newValue;
        }
    }

    let csvContent = "data:text/csv;charset=utf-8,";
    let keys = Object.keys(data[0]);
    for (let i=0; i<keys.length; i++) {
        let key = keys[i];
        csvContent += key + ',';
    }
    csvContent += "\n";
    data.forEach(function(rowObject){
        let row = "";
        for (let i=0; i<keys.length; i++) {
            let key = keys[i];
            row += rowObject[key] + ',';
        }

       csvContent += row + "\r\n";
    });
    let encoded = encodeURI(csvContent);
    window.open(encoded);
}